package com.AIS.Authentication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.AIS.Authentication.Entity.RoleUser;

public interface RoleRepository  extends JpaRepository<RoleUser, Integer>{

}
