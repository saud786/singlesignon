package com.AIS.Authentication.repository;


import com.AIS.Authentication.Entity.UserOTP;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OtpRepository extends JpaRepository<UserOTP, String> {

    Optional<UserOTP> findByUserName(String userName);
}
