package com.AIS.Authentication.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.AIS.Authentication.Entity.CoreUser;

@Repository
public interface UserRepository extends JpaRepository<CoreUser, Integer> {
	Optional<CoreUser> findByUserName(String username);

}
