package com.AIS.Authentication.service;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@EnableAsync
public class MailSenderService {

    /**
     * From address
     */
    @Value("${support.email.from}")
    private String from;

    /**
     * Support mail to
     */


    /**
     * Java mail sender
     */
    @Value("${supportcc.email.to}")
    private String MailCCTo;

    @Autowired
    private JavaMailSender mailSender;


    /**
     * <b>Send to</b>
     * <p>
     * // * @param from Sender of the Mail
     *
     * // * @param to Receiver Of Mail // * @param subject Subject of the mail //s
     * * @param msg Body of Mail
     *
     * @return Success or Not
     * @throws MessagingException           the messaging exception
     * @throws UnsupportedEncodingException
     */

    public String sendTo(String OTP ,String email)
            throws MessagingException, UnsupportedEncodingException {

        String message = "";
        String msg = "Dear Sir / Madam \n " + "\n" + "\n" + "<p>You have recieved OTP for Authentication\n</p>" + "\n" + "\n"
                + "\n" + "<p>Please find OTP below:  \n</p>" +"<b>"+OTP +"</b>"+ "\n </p>"
                + "\n"  + "\n" + "\n" + " \n" + " \n" + "\n" + "<p>Regards</p>\n" + "<p>AIS IT TEAM</p>";




        String subject = "OTP Authentication";
        MimeMessage mimMessage = mailSender.createMimeMessage();
        InternetAddress fromAddress = new InternetAddress(from, "AIS-IT");
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(mimMessage, true);
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(msg, true);
            helper.setFrom(fromAddress);
            // helper.setCc(MailCCTo);
            mailSender.send(mimMessage);
            log.info("Mail sent ");
        } catch (MessagingException e) {
            e.printStackTrace();
            log.error("Error in mailing service " + e.getMessage());
        }

        return message;
    }

}
