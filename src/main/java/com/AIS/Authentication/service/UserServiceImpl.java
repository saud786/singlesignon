package com.AIS.Authentication.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AIS.Authentication.Entity.CoreUser;
import com.AIS.Authentication.Entity.ResponseStatus;
import com.AIS.Authentication.repository.UserRepository;
import com.AIS.Authentication.utils.PGPCrypto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl {

	@Autowired
	private UserRepository repo;

	@Autowired
	private PGPCrypto pgpCrypto;

	public ResponseStatus<CoreUser> saveUser(CoreUser user) {

		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy/MM/dd").format(date);
		CoreUser coreUser = new CoreUser();

		coreUser = user;

		ResponseStatus<CoreUser> body = new ResponseStatus<CoreUser>();
		Cipher cipherDecrypt = this.pgpCrypto.cipherEncrypt();
		try {
			Optional<CoreUser> user1 = repo.findByUserName(user.getUserName());

			if (user1.isPresent()) {
				log.info("User Is Already Present");
				body.setBody(null);
				body.setDescription("User Already saved in Database");
				body.setStatus(false);
			} else {

				coreUser.setCreatedDate(currentDate);
				coreUser.setUpdatedDate(currentDate);
				coreUser.setPassword(pgpCrypto.encrypt(user.getPassword(), cipherDecrypt));
				repo.save(coreUser);
				body.setBody(coreUser);
				body.setDescription("User Saved Succesfully");
				body.setStatus(true);
			}

		} catch (Exception e) {
			log.error("Error in user saving: " + e.getMessage());
			body.setDescription("User Not saved");
			body.setStatus(false);
		}

		return body;

	}

	public ResponseStatus<CoreUser> getUserById(int id) {

		ResponseStatus<CoreUser> body = new ResponseStatus<CoreUser>();
		Cipher cipherDecrypt = this.pgpCrypto.cipherDecrypt();
		try {
			CoreUser coreUser = new CoreUser();
			coreUser = repo.findById(id).get();
			coreUser.setPassword(this.pgpCrypto.decrypt(coreUser.getPassword(), cipherDecrypt));

			body.setBody(coreUser);
			body.setDescription("User fetched Succesfully");
			body.setStatus(true);
		} catch (Exception e) {
			log.error("Error in user fetching: " + e.getMessage());
			body.setDescription("User Not fetched");
			body.setStatus(false);
		}

		return body;
	}

	public ResponseStatus<CoreUser> checkUser(String userName, String password) {
		ResponseStatus<CoreUser> body = new ResponseStatus<CoreUser>();
		Cipher cipherDecrypt = this.pgpCrypto.cipherDecrypt();

		try {

			Optional<CoreUser> user1 = repo.findByUserName(userName);
			String encPassword = this.pgpCrypto.decrypt(user1.get().getPassword(), cipherDecrypt);
			if (user1.isPresent() && encPassword.equals(password) && user1.get().getLdapStatus() == 0) {
			//	user1.get().setPassword("");

				body.setBody(user1.get());
				body.setDescription("User Found in Database");
				body.setStatus(true);
			} else {
				body.setBody(null);
				body.setDescription("User  not Found in Database");
				body.setStatus(false);

			}

		} catch (Exception e) {
			log.error("Error in user fetching: " + e.getMessage());
			body.setDescription("User Not fetched");
			body.setStatus(false);
		}

		return body;

	}

	public ResponseStatus<CoreUser> updateUser(CoreUser user) {

		Date date = new Date();
		String currentDate = new SimpleDateFormat("yyyy/MM/dd").format(date);
		CoreUser coreUser = new CoreUser();

		coreUser = user;

		ResponseStatus<CoreUser> body = new ResponseStatus<CoreUser>();
		Cipher cipherDecrypt = this.pgpCrypto.cipherEncrypt();
		try {
			Optional<CoreUser> user1 = repo.findByUserName(user.getUserName());

			if (user1.isPresent()) {
				coreUser.setCreatedDate(user1.get().getCreatedDate());
				coreUser.setUpdatedDate(currentDate);
				coreUser.setPassword(pgpCrypto.encrypt(user.getPassword(), cipherDecrypt));
				repo.save(coreUser);
				coreUser.setPassword("");
				body.setBody(coreUser);
				body.setDescription("User Updated Succesfully");
				body.setStatus(true);
			} else {
				body.setDescription("User Not found in DB ");
				body.setStatus(false);

			}

		} catch (Exception e) {
			log.error("Error in user saving: " + e.getMessage());
			body.setDescription("User Not saved");
			body.setStatus(false);
		}

		return body;

	}
	
	public ResponseStatus<List<CoreUser>> getAllUsers() {

		ResponseStatus<List<CoreUser>> body = new ResponseStatus<List<CoreUser>>();
		Cipher cipherDecrypt = this.pgpCrypto.cipherDecrypt();
		try {
			List<CoreUser> coreUser ;
		
		coreUser = repo.findAll();
		
		coreUser.stream().forEach(x ->x.setPassword(this.pgpCrypto.decrypt(x.getPassword(), cipherDecrypt)));
			//coreUser.setPassword(this.pgpCrypto.decrypt(coreUser.getPassword(), cipherDecrypt));

			body.setBody(coreUser);
			body.setDescription("User fetched Succesfully");
			body.setStatus(true);
		} catch (Exception e) {
			log.error("Error in user fetching: " + e.getMessage());
			body.setDescription("User Not fetched");
			body.setStatus(false);
		}

		return body;
	}

}
