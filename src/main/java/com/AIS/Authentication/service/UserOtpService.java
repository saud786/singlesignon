package com.AIS.Authentication.service;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AIS.Authentication.Entity.ResponseStatus;
import com.AIS.Authentication.Entity.UserOTP;
import com.AIS.Authentication.repository.OtpRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserOtpService {

	@Autowired
	private OtpRepository repo;

	@Autowired
	private MailSenderService mailService;

	public ResponseStatus checkOtp(UserOTP userOTP) {
		Optional<UserOTP> user = repo.findByUserName(userOTP.getUserName());
		ResponseStatus responseStatus = new ResponseStatus();
		String message = null;

		try {
			if (user.isPresent()) {
				if (user.get().getOtpKey().equals(userOTP.getOtpKey().trim())) {

					message = "OTP Verified";
					repo.delete(userOTP);
					responseStatus.setDescription(message);
					responseStatus.setStatus(true);

				} else {

					message = "OTP not verified";
					responseStatus.setDescription(message);
					responseStatus.setStatus(false);
				}
			} else {
				message = "OTP details not found";
				responseStatus.setDescription(message);
				responseStatus.setStatus(false);
			}

		} catch (Exception e) {
			log.error("Exception in check OTP method : " + e.getMessage());

		}
		return responseStatus;

	}

	public void save(UserOTP userOTP) throws MessagingException, UnsupportedEncodingException {
		Optional<UserOTP> user = repo.findByUserName(userOTP.getUserName());
		try {
			if (user.isPresent()) {

				repo.delete(userOTP);

				repo.save(userOTP);

			} else {

				repo.save(userOTP);
				log.info("OTP Details saved");

			}
		} catch (Exception e) {

			log.error("Exception in save otp method : " + e.getMessage());

		}

		mailService.sendTo(userOTP.getOtpKey(), userOTP.getUserName());

	}
}
