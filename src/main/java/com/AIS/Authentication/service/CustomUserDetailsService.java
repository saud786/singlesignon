//package com.AIS.Authentication.service;
//
//import java.util.Collection;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.AuthorityUtils;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.AIS.Authentication.Entity.CustomUser;
//import com.AIS.Authentication.Entity.ICustomUserDetails;
//import com.AIS.Authentication.Entity.User;
//import com.AIS.Authentication.exceptions.UserNotActiveException;
//import com.AIS.Authentication.repository.UserRepository;
//
//import javax.transaction.Transactional;
//
//@Service
//@Transactional
//public class CustomUserDetailsService implements UserDetailsService {
//
//	@Autowired
//	private UserRepository userRepository;
//
//	@Override
//	public ICustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		User user = userRepository.findByEmail(username)
//				.orElseThrow(() -> new UsernameNotFoundException("Email " + username + " not found"));
//		if(!user.isEnabled()) {
//			throw new UserNotActiveException("User is not active");
//		}
//
//		return new CustomUser(user.getEmail(), user.getPassword(),
//				getAuthorities(user),user.getId());
//	}
//
//	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
//		String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
//		Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
//		return authorities;
//	}
//
//}
