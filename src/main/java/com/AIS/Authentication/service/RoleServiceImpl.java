package com.AIS.Authentication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.AIS.Authentication.Entity.RoleUser;
import com.AIS.Authentication.repository.RoleRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoleServiceImpl {

	
	@Autowired
	private RoleRepository repo;
	
	
	public RoleUser saveRole(RoleUser role) {
		
		repo.save(role);
		return role;
		
	}
}
