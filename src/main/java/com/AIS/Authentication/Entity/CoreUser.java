package com.AIS.Authentication.Entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "core_user")
@Data
public class CoreUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
    
    private String userName;
    private String fullName;
    private int  ldapStatus = 0;
    private String password;
    private String location;
    private String department;
    private String createdDate;
    private String updatedDate;
    private String lastLoginTime;
    private String position;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<RoleUser>roles;
    
   

}
