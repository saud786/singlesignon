package com.AIS.Authentication.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "roles")
@Data
public class RoleUser {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
    
    private String roleName;
   
}
