package com.AIS.Authentication.Entity;

import org.springframework.security.core.userdetails.UserDetails;

public interface ICustomUserDetails  extends UserDetails {
	public String getId();

}
