package com.AIS.Authentication.Entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_otp")
@Data
public class UserOTP {

    private String id;
    @Id
    private String userName;
    private String otpKey;
    private int  status;

}
