package com.AIS.Authentication.Entity;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class CustomUser extends org.springframework.security.core.userdetails.User implements ICustomUserDetails {
	private static final long serialVersionUID = 1L;
	private String id;
	
	public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities,String aisid) {
		super(username, password, authorities);
		this.id=aisid;
	}

	public String getId() {
		return id;
	}
}
