package com.AIS.Authentication.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailConfig {

    @Value("${smtp.email.user}")
    private String username;

    @Value("${smtp.email.password}")
    private String password;

    @Value("${smtp.email.port}")
    private int port;

    @Value("${smtp.email.host}")
    private String host;

    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties prop = mailSender.getJavaMailProperties();
        prop.put("mail.transport.protocol", "smtp");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.enable", "true");
        prop.put("mail.debug", "false");
        //prop.put("mail.smtp.localhost", host);
        prop.put("mail.smtp.starttls.enable", "true");

        return mailSender;
    }

}
