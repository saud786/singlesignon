package com.AIS.Authentication.security;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LDAPAuthenticationProvider {

	private static final String LDAP_FILTER = "(&(objectClass=*) (sAMAccountName=%s))";

	@Value("${ldap.timeout}")
	private int timeout;

	@Value("${ldap.url}")
	private String ldapURL;

	@Value("${ldap.context.factory}")
	private String contextFactory;

	@Value("${ldap.security.authentication}")
	private String authentication;

	@Value("${ldap.domain}")
	private String domain;

	@Value("${ldap.domain.content}")
	private String domainContent;

	@Value("${ldap.attributes}")
	private String[] attributes;

	public JSONObject searchUser(String userName, String password) {
		DirContext context = null;
		JSONObject response = new JSONObject();
		try {
			context = getContext(userName, password);
			final NamingEnumeration<SearchResult> answer = context.search(domainContent,
					"(& (userPrincipalName=" + userName + ")(objectClass=user))", searchControles());
			if (answer.hasMoreElements()) {
				while (answer.hasMoreElements()) {
					final SearchResult result = answer.nextElement();
					final Hashtable<String, String> env = new Hashtable<>(6);
					env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
					env.put(Context.PROVIDER_URL, ldapURL);
					env.put(Context.SECURITY_AUTHENTICATION, authentication);
					env.put(Context.SECURITY_PRINCIPAL, result.getNameInNamespace());
					env.put(Context.SECURITY_CREDENTIALS, password);
					env.put("com.sun.jndi.ldap.connect.timeout", String.valueOf(timeout));
					new InitialDirContext(env);
					final NamingEnumeration<String> attrs = result.getAttributes().getIDs();
					String value = null;
					while (attrs.hasMoreElements()) {
						final String id = attrs.nextElement();
						value = (result.getAttributes().get(id) != null) ? result.getAttributes().get(id).toString()
								: "";
						if (value != "") {
							response.put(id, removeKey(value));
						}
					}
				}
			}
		} catch (Exception ex) {
			log.error("Error while connecting LDAP server " + ldapURL.trim() + " for user " + userName, ex);
			throw new UsernameNotFoundException(
					"Error while connecting LDAP server " + ldapURL.trim() + " for user " + userName, ex);
		} finally {
			if (context != null)
				try {
					context.close();
				} catch (NamingException ne) {
					log.error("Error while closing the connection", ne);
				}

		}
		return response;
	}

	private String removeKey(String value) {
		return value.substring(value.indexOf(":") + 1).trim();
	}

	private SearchControls searchControles() {
		final SearchControls searchControls = new SearchControls();
		searchControls.setDerefLinkFlag(true);
		searchControls.setTimeLimit(timeout);
		searchControls.setReturningAttributes(attributes);
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		return searchControls;
	}

	private DirContext getContext(String userId, String password) throws NamingException {
		final Hashtable<String, String> env = new Hashtable<>(6);

		env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
		env.put(Context.PROVIDER_URL, ldapURL);
		env.put(Context.SECURITY_AUTHENTICATION, authentication);
		env.put(Context.SECURITY_PRINCIPAL, userId);
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put("com.sun.jndi.ldap.connect.timeout", String.valueOf(timeout));
		return new InitialDirContext(env);
	}
}
