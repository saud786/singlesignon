package com.AIS.Authentication.security.model;

import static java.util.Arrays.asList;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class RestErrorList extends ArrayList<ErrorMessage> {

	private HttpStatus status;

	public RestErrorList(HttpStatus status, ErrorMessage... errors) {
		this(status.value(), errors);
	}

	public RestErrorList(int status, ErrorMessage... errors) {
		super();
		this.status = HttpStatus.valueOf(status);
		addAll(asList(errors));
	}

}
