package com.AIS.Authentication.utils;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PGPCrypto {
	
	@Value("${aesKey}")
	private String aesKey;

	public String encrypt(String strToEncrypt, Cipher cipher) {
		try {
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			log.error("{HRIS financial data} Error while encrypting - " + e.getMessage());
		}
		return null;
	}

	public Cipher cipherEncrypt() {
		try {
			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivspec);
			return cipher;
		} catch (Exception e) {
			log.error("{HRIS financial data} Error in cipherEncrypt - " + e.getMessage());
		}
		return null;
	}

	public String decrypt(String strToDecrypt, Cipher cipher) {
		try {
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			log.error("{HRIS financial data} Error while decrypting - " + e.getMessage());
		}
		return null;
	}

	public Cipher cipherDecrypt() {
		try {
			byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			IvParameterSpec ivspec = new IvParameterSpec(iv);

			SecretKeySpec secretKey = new SecretKeySpec(aesKey.getBytes(), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
			return cipher;
		} catch (Exception e) {
			log.error("{HRIS financial data} Error in cipherEncrypt - " + e.getMessage());
		}
		return null;
	}


}
