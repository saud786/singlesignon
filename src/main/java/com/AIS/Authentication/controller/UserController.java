package com.AIS.Authentication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.AIS.Authentication.Entity.CoreUser;
import com.AIS.Authentication.Entity.ResponseStatus;
import com.AIS.Authentication.Entity.UserOTP;
import com.AIS.Authentication.service.UserServiceImpl;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/user")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
	
	@Autowired
	private UserServiceImpl userService;
	
	
	

    @PostMapping("/save")
    public ResponseStatus<CoreUser> saveUser(@RequestBody CoreUser user) {

         


        return userService.saveUser(user);
    }

    @GetMapping("/byId/{id}")
    public ResponseStatus<CoreUser> getUserById(@PathVariable int id) {
    	
    	return userService.getUserById(id);
    }
    
    @PostMapping("/update")
    public ResponseStatus<CoreUser> updateUser(@RequestBody CoreUser user) {

         


        return userService.updateUser(user);
    }
    
    @GetMapping("/getAll")
    public ResponseStatus<List<CoreUser>> getAllUser() {
    	
    	return userService.getAllUsers();
    }
    
}
