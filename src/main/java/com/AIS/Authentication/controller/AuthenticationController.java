package com.AIS.Authentication.controller;

import static com.AIS.Authentication.security.JwtUtils.generateHMACToken;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.AIS.Authentication.Entity.ResponseStatus;
import com.AIS.Authentication.Entity.UserOTP;
import com.AIS.Authentication.exceptions.UserNotActiveException;
import com.AIS.Authentication.security.LDAPAuthenticationProvider;
import com.AIS.Authentication.security.model.AuthenticationRequest;
import com.AIS.Authentication.security.model.AuthenticationResponse;
import com.AIS.Authentication.security.model.ResponseWrapper;
import com.AIS.Authentication.service.UserOtpService;
import com.AIS.Authentication.service.UserServiceImpl;
import com.nimbusds.jose.JOSEException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/auth")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

	@Value("${app.jwtExpirationInMs}")
	private int expirationInMinutes;

	@Autowired
	private LDAPAuthenticationProvider ldapAuthenticationProvider;

	@Autowired
	private UserOtpService otpService;

	@Autowired
	private UserServiceImpl userSerImpl;

	@Autowired
	private AuthenticationManager authenticationManager;

	@SuppressWarnings("unused")
	@PostMapping("/authenticate")
	public AuthenticationResponse authenticateUser(@RequestBody AuthenticationRequest user) {
		AuthenticationResponse resp = new AuthenticationResponse();
		String username = user.getUsername().trim().replace(".com", ".net");
		ResponseStatus<?> body = userSerImpl.checkUser(user.getUsername().trim(), user.getPassword().trim());

		if (body.getStatus() == true) {
			JSONObject jsonObject = new JSONObject(body.getBody());
			try {
				resp = this.checkUserInDB(user, jsonObject);
			} catch (Exception e) {
				log.error("Error in checking user in DB" + e.getMessage());
				resp.setMessage("error=" + e.getMessage());
				resp.setStatus(false);
			}

		} else {
			JSONObject ldapResponse = ldapAuthenticationProvider.searchUser(username, user.getPassword().trim());
			String token = "";
			try {
				if (ldapResponse != null && ldapResponse.length() > 0) {

					// ICustomUserDetails userDetails =
					// this.customUserDetailsService.loadUserByUsername(user.getUsername());
					Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), "", null);
					SecurityContextHolder.getContext().setAuthentication(auth);
					token = this.generateAuthToken(auth, user);
					String[] managerarray = ldapResponse.getString("manager").split(",OU");
					String manager = managerarray[0].replace("CN=", "");
					resp.setToken(token);
					resp.setStatus(true);
					resp.setEmail(ldapResponse.getString("mail"));
					resp.setLocation(ldapResponse.getString("physicalDeliveryOfficeName"));
					resp.setDepartment(ldapResponse.getString("department"));
					resp.setName(ldapResponse.getString("cn"));
					resp.setManager(manager.replace("\\", ""));
					resp.setMessage("user_Authenticated_Succesfully");
					UserOTP userOTP = new UserOTP();
					userOTP.setOtpKey(getRandomNumberString());
					userOTP.setUserName(user.getUsername().trim());
					otpService.save(userOTP);
					// resp.setAis_id(userDetails.getId());
					// resp.setAuthorities(userDetails.getAuthorities());

				} else {
					resp.setMessage("username/password is not correct");
					resp.setStatus(false);
				}

			} catch (UserNotActiveException e) {
				resp.setMessage(e.getMessage());
				resp.setStatus(false);
			} catch (UsernameNotFoundException e) {
				resp.setMessage("user/password is not correct");
				resp.setStatus(false);
			} catch (Exception e) {
				resp.setMessage("error=" + e.getMessage());
				resp.setStatus(false);
			}
		}
		return (resp);
	}

	@GetMapping("/secureapi")
	public ResponseWrapper secureapi() {
		// fake class just for testing purpose
		@AllArgsConstructor
		@Data
		class SensitiveInformation {
			String foo;
			String bar;
		}

		return new ResponseWrapper(new SensitiveInformation("ABCD", "XYZ"), null, null);
	}

	private String generateAuthToken(Authentication auth, AuthenticationRequest user)
			throws IOException, JOSEException {
		String secret = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("secret.key"),
				Charset.defaultCharset());
		String token = generateHMACToken(user.getUsername(), auth.getAuthorities(), secret, this.expirationInMinutes);
		return token;
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/checkOtp")
	public ResponseStatus checkOtp(@RequestBody UserOTP userOTP) {

		ResponseStatus body = otpService.checkOtp(userOTP);

		return body;
	}

	public static String getRandomNumberString() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);

		// this will convert any number sequence into 6 character.
		return String.format("%06d", number);
	}

	private AuthenticationResponse checkUserInDB(AuthenticationRequest user, JSONObject jsonObject) throws Exception {
		AuthenticationResponse resp = new AuthenticationResponse();
		Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), "", null);
		SecurityContextHolder.getContext().setAuthentication(auth);
		String token = generateAuthToken(auth, user);

		resp.setToken(token);
		resp.setStatus(true);
		resp.setEmail(jsonObject.getString("userName"));
		resp.setLocation(jsonObject.getString("location"));
		// resp.setDepartment(body.getBody().getClass().getField("department").toString());
		resp.setName(jsonObject.getString("fullName"));
		resp.setManager("");
		resp.setMessage("user_Authenticated_Succesfully from Database");
		UserOTP userOTP = new UserOTP();
		userOTP.setOtpKey(getRandomNumberString());
		userOTP.setUserName(user.getUsername().trim());
		otpService.save(userOTP);

		return resp;

	}

}
